# -*- coding: utf-8 -*-

"""
Utilities to do evaluations 

@author Animesh Prasad
@data March 2018
"""

import numpy as np
import pickle as p

from sklearn.metrics import recall_score, precision_score, f1_score
from sklearn.metrics import confusion_matrix
from functools import reduce

def calculate_precision_recall_f1(y_true, y_pred, average=None):
    print ('Recall  %s' %recall_score(y_true=y_true, y_pred=y_pred, average=average))
    print ('Precision %s' %precision_score(y_true=y_true, y_pred=y_pred, average=average))
    print ('F1 %s' %f1_score(y_true=y_true, y_pred=y_pred, average=average))

def print_confusion_matrix(y_true, y_pred):
    print ('Confusion Matrix \n %s' %confusion_matrix(y_true, y_pred))

def calculate_metrics(y_true, y_pred, ignore=None, average=None):
    """
    calculated the metrices, but before flatten the lists, preprocess to ignore 
    non important (negative) classes, the integer index is required as ignore

    This should be used in case the labels are already crossed during traning.
    """
    y_true = reduce(lambda x,y: np.hstack((x,y)), y_true)
    y_pred = reduce(lambda x,y: np.hstack((x,y)), y_pred)
    assert(y_true.shape[0] == y_pred.shape[0])
    if ignore is not None:
        indices_to_ignore = []
        for i in range(y_true.shape[0]):
            if y_true[i] == ignore and y_pred[i] == ignore:
                indices_to_ignore.append(i)
        y_true = np.delete(y_true, indices_to_ignore, None)
        y_pred = np.delete(y_pred, indices_to_ignore, None)
        assert not(any(a==ignore and b==ignore for a,b in zip(y_true, y_pred)))
    calculate_precision_recall_f1(y_true, y_pred, average)
    print_confusion_matrix(y_true, y_pred) 
    
def calculate_metrics_multitask(y_true_tuple, y_pred_tuple, ignore=None):
    """
    Get joint results incase the traning was done per task and not jointly
    """
    for tasks in len(y_true_tuple):
        # Get individual task results
        calculate_metrics(y_true_tuple[tasks], y_pred_tuple[tasks], ignore)


if __name__ == "__main__":
    FOLDS = 10
    for model_name in ['CCRF', 'ECRF',  'GCRF', 'EFGCRF', 'NTEFGCRF', 'crf', 'blstm',
                       'blstm_crf', 'blstm_cnn_we', 'crf_we', 'blstm_we', 'blstm_crf_we']:
        for label_type in ['cat', 'oth', 'joint']:
            for train_name in ['true', 'noisy', 'mix']:
                for test_name in ['true', 'noisy']:
                    try:
                        _y_t = []
                        _y_pr = []
                        for folds in range(FOLDS):
                            try:
                                with open ('outputs/' + model_name + '_' + label_type + '_'+  train_name + 'ON' + test_name  + '_' + str(folds), 'rb') as fp:
                                    y_t, y_pr = p.load(fp)
                                    _y_t.append(y_t)
                                    _y_pr.append(y_pr)
                            except:
                                pass
                        _y_t = np.hstack(_y_t) 
                        _y_pr = np.hstack(_y_pr)
                        print('\n\n\n Result for all folds of ' + model_name + '_' + label_type + '_'+  train_name + 'ON' + test_name + '\n')
                        calculate_metrics(_y_t, _y_pr, average='micro', ignore=None)
                    except: 
                        pass
                          
     #y_true=[np.array([1,0,0,3], dtype=np.int32), np.array([2,1,1], dtype=np.int32)]
     #y_pred=[np.array([0,0,0,2], dtype=np.int32), np.array([2,0,1], dtype=np.int32)]
     #calculate_metrics(y_true, y_pred, ignore=0)
