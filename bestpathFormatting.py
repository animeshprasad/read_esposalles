# -*- coding: utf-8 -*-
"""
    @author: Hervé Déjean
    copy Naver labs europe 2018 
    
    formatting for htr output
    
    input: one line per record
    output: page_id_recordid_lineNumber : bestpath
    
    Divendres al pr rebere de mr Rafel ferra mercader⏎de Bara fill de mr Pere ferran mercader y de Hiero⏎nyma defuncta ab la Sra Elena donsella filla de jau⏎me lledo mercade
r de Girona y de Hieronyma defuncts

    
    idPage10354_Record3_Line0:Dimars a 2 rebere de Pere fabregas tavarner viudo de

    /nfs/project/read/data/IEHHR2017/Citlab/out.tar/out
    cd train
    for i in id* ; do echo $i;~/anaconda3/bin/python ~/READ-Esposalles/Esposalles/bestpathFormatting.py $i >>bestpath.txt;done
    cd ../val/
    for i in id* ; do echo $i;~/anaconda3/bin/python ~/READ-Esposalles/Esposalles/bestpathFormatting.py $i >>bestpath.txt;done
    cp train/bestpath.txt  train_bestpath.txt
    cp val/bestpath.txt  val_bestpath.txt
    cat train_bestpath.txt  val_bestpath.txt > bestpath.txt

    differences between transcription and bestpath  :idPage10375_Record2 missing in Citlab data
    $ diff tid bid
    560,562d559
    < idPage10375_Record2_Line0
    < idPage10375_Record2_Line1
    < idPage10375_Record2_Line2

    @author: Animesh

    /nfs/project/read/data/IEHHR2017/Citlab/out.tar/out
    cd test
    for i in id* ; do echo $i; python /nfs/project/read/animesh/READ_Esposalles/bestpathFormatting.py $i >>bestpath.txt;done
    cp test/bestpath.txt blind_test_bestpath.txt

    
"""

import sys,os


if __name__ == "__main__":
    
    try:sFolder=sys.argv[1]
    except IndexError: print('usage: bestpathFormatting <RECORDFOLDER>');sys.exit(1)
    try:o=open(os.path.join(sFolder,'bestpath.txt'),encoding='utf-8')
    except IOError: print('%s not found'%sFolder);sys.exit(1)
    
    line = o.readline()
    o.close()
    lLines=line.split('⏎')
    for i,line in enumerate(lLines):
        print(sFolder+"_Line%d:%s"%(i,line))
    
