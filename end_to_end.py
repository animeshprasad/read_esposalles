# -*- coding: utf-8 -*-

"""
End to end code for running the best model (found during develpment)

@author Animesh Prasad
@data May 2018
"""
import numpy as np
import pickle as p

from utils import make_node_features as ngramfeatures
from utils_neural import make_node_features
from utils_neural import do_label_product, do_label_linearisation, open_tuples, make_tuples
from Esposalles.Record import Record
from Esposalles.Alignment import Transcript
from Esposalles.Feature import Feature

from model_neural import  blstm, predict, blstm_multitask, predict_multitask

C = .1
FOLDS = 1

def discard_padding(y_pr, y_t):
    list_to_return = []
    for items_pred, items_true in zip(y_pr, y_t):
        list_to_return.append(items_pred[:len(items_true)]) 
    return list_to_return

def transform_to_onehot(y_tr, class_length):
    list_to_return = []
    for items in y_tr:
        b =  np.zeros((items.shape[0], class_length))
        b[np.arange(items.shape[0]), items] = 1
        list_to_return.append(b)
    return list_to_return

def transform_from_onehot(y_p, class_length):
    list_to_return = []
    for items in y_p:
        a = np.array([np.where(r==max(r))[0][0] for r in items])
        list_to_return.append(a)
    return list_to_return

def do_padding(unpadded_list, depth=2, max_list_per_depth=[30, 10]):
    sample_lists = []
    for samples in unpadded_list:
        token_feature_list = []
        if depth == 2: 
            for tokens in samples:
                l = np.zeros(max_list_per_depth[1], dtype=np.int32)
                l[:len(tokens)] = np.array(tokens)
                token_feature_list.append(l)
            sample_lists.append(np.array(token_feature_list +  [np.zeros(max_list_per_depth[1], dtype=np.int32) for i in range(max_list_per_depth[0] - len(token_feature_list))]))
        elif depth == 1:
            l = np.zeros(max_list_per_depth[0], dtype=np.int32)
            l[:len(samples)] = np.array(samples)
            sample_lists.append(l)
    return np.array(sample_lists)


def test(extractor):
    test_transcript = Transcript(0)
    ids, text =  test_transcript.load_transcript("data", transcript_type='blind_test_bestpath.txt')
    with open('outputs2/test_output', 'w') as f:
        for _i, _trans in zip(ids, text):
            for _lineno,_l in enumerate(_trans.llsW):
                for _wordno,sW in enumerate(_l):
                    f.write(_i+'_Line'+str(_lineno)+'_Word'+str(_wordno)+ ','+ sW+'\n')
                #f.write('\n') 
     
    text =  [ items.flatten() for items in text]
    features = Feature()
    if extractor:
        features.set_feature_extractor(extractor)
    X=np.array(features.fit_on_X(text))
    return (ids, text, X)


def prepare_outputs(file1, file2, basefile, outfile='outputs2/final_transcript'):

    def split_files(outfile, outline='outputs2/line/', outword='outputs2/word/'):
        with  open(outfile, 'r') as of:
            all_lines = of.readlines()
            start_id  = all_lines[0].split(',')[0]
            record_id = '_'.join(start_id.split('_')[:2])
            f_line  =  open(outline + record_id + '_output.csv', 'w')
            f_word = open(outword + record_id + '_output.csv', 'w')
            for i in range(len(all_lines)):
                k = '_'.join(all_lines[i].split(',')[0].split('_')[:2])
                if k == record_id:
                     f_line.write( '_'.join(all_lines[i].split('_')[:3]) + ',' + ','.join(all_lines[i].split(',')[1:]))
                     f_word.write(all_lines[i])
                else:
                     start_id  = all_lines[i].split(',')[0]
                     record_id = '_'.join(start_id.split('_')[:2])
                     f_line.close()
                     f_word.close()
                     f_line  =  open(outline + record_id + '_output.csv', 'w')
                     f_word = open(outword + record_id + '_output.csv', 'w')
                     f_line.write( '_'.join(all_lines[i].split('_')[:3]) + ',' + ','.join(all_lines[i].split(',')[1:]))
                     f_word.write(all_lines[i])

    with open(file1, 'r') as f1, open(file2, 'r') as f2,\
         open(basefile, 'r') as bf, open(outfile, 'w') as of:
        token_t1 = 'dummy_start'
        while token_t1:
            token_t1 = f1.readline().split(',')
            token_t2 = f2.readline().split(',')
            base_token = bf.readline().split(',')
            if token_t1[0] =='':
                of.close()
                split_files(outfile)
                exit()
            if token_t1[0] == token_t2[0] and token_t2[1] == base_token[1].strip():
                if token_t1[2].strip() != 'other' and token_t2[2].strip() != 'none':
                    of.write(base_token[0].strip() + ','+ base_token[1].strip() + ',' + token_t1[2].strip() + ',' + token_t2[2])
            else:
                print (token_t1, token_t2, base_token)
                print ('ERROR, Exiting now!')
                exit()


def train(model_id=-1):

    def augment_data(data_list):
        a = []
        for item in data_list:
            for i in item:
                a.append(np.array(i))
        return np.array(a)
   
    rec = Record(0)
    dCATEGORY_index = {v: k for k, v in rec.dCATEGORY_index.items()}
    dPERSON_index = {v: k for k, v in rec.dPERSON_index.items()}

    class_dict = {'cat':dCATEGORY_index, 'oth':dPERSON_index}


    for label_type in class_dict.keys():
        class_length = len(Record(0).class_dict[label_type])
        for folds in range (FOLDS):
            if model_id == -1 or model_id == 0:
                x_tr, x_v, x_t, y_tr, y_v, y_t, feature_extractor = ngramfeatures(label_type, folds)
                xN_tr, xN_v, xN_t, yN_tr, yN_v, yN_t, feature_extractor = ngramfeatures(label_type, folds, feature_extractor, True)
                ids, text, X = test(feature_extractor)                

                train_set = {
                             'mix' : (augment_data([x_tr, x_t, xN_tr, xN_t]), augment_data([y_tr, y_t, yN_tr, yN_t]))
                            }

                test_set = {
                            'noisy' : (X, [])
                           }

                for train_name, (x_tr, y_tr) in train_set.items():
                    y_tr = transform_to_onehot(y_tr, class_length)
                    for model_name_str, model_name in {'blstm':blstm }.items():
                        model = model_name(feature_extractor.get_feature_names(), class_length, x_tr, y_tr)
                        for test_name, (x_t, y_t) in test_set.items():
                            y_pr = predict(model, x_t)
                            y_pr = transform_from_onehot(y_pr, class_length)
                            with open('outputs2/' +  model_name_str + '_' + label_type + '_' + train_name + 'ON' + test_name  + '_' + str(folds), 'w') as fp:
                                for i, items in enumerate(text):
                                    for j, tokens in enumerate(items):
                                        fp.write(ids[i] + ',' + tokens + ',')
                                        fp.write((class_dict[label_type][y_pr[i][j]]) + '\n')


    class_length1 = len(Record(0).class_dict['cat'])
    class_length2 = len(Record(0).class_dict['oth'])

    for folds in range (FOLDS):
        x1_tr, x1_v, x1_t, y1_tr, y1_v, y1_t, feature_extractor = ngramfeatures('cat', folds)
        _, _, _, y2_tr, y2_v, y2_t, _ = ngramfeatures('oth', folds)
        xN1_tr, xN1_v, xN1_t, yN1_tr, yN1_v, yN1_t, feature_extractor = ngramfeatures('cat', folds, feature_extractor, True)
        _, _, _, yN2_tr, yN2_v, yN2_t, _ = ngramfeatures('oth', folds, feature_extractor, True)
        ids, text, X = test(feature_extractor)

        train_set = {
                     'mix' : (augment_data([x1_tr, x1_t, xN1_tr, xN1_t]), augment_data([y1_tr, y1_t, yN1_tr, yN1_t]), augment_data([y2_tr, y2_t, yN2_tr, yN2_t]))
                    }

        test_set = {
                    'noisy' : (X, [], [])
                   }

        label_type =  'joint'
        if model_id == -1 or model_id == 2:
            for train_name, (x1_tr, y1_tr, y2_tr) in train_set.items():
                y1_tr = transform_to_onehot(y1_tr, class_length1)
                y2_tr = transform_to_onehot(y2_tr, class_length2)
                for test_name, (x1_t, y1_t, y2_t) in test_set.items():
                    model = blstm_multitask(feature_extractor.get_feature_names(), class_length1, class_length2, x1_tr, y1_tr, y2_tr)
                    y1_pr, y2_pr = predict_multitask(model, x1_t)
                    y1_pr = transform_from_onehot(y1_pr, class_length1)
                    y2_pr = transform_from_onehot(y2_pr, class_length2)
                    with open('outputs2/' +  'blstm_multitask' + '_' + 'cat' + '_' + train_name + 'ON' + test_name  + '_' + str(folds), 'w') as fp:
                        for i, items in enumerate(text):
                            for j, tokens in enumerate(items):
                                fp.write(ids[i] + ',' + tokens + ',')
                                fp.write((class_dict['cat'][y1_pr[i][j]]) + '\n')
                    with open('outputs2/' +  'blstm_multitask' + '_' + 'oth' + '_' + train_name + 'ON' + test_name  + '_' + str(folds), 'w') as fp:
                        for i, items in enumerate(text):
                            for j, tokens in enumerate(items):
                                fp.write(ids[i] + ',' + tokens + ',')
                                fp.write((class_dict['oth'][y2_pr[i][j]]) + '\n')



if __name__ == "__main__":
    train(-1)
    prepare_outputs('outputs2/blstm_cat_mixONnoisy_0' , 'outputs2/blstm_oth_mixONnoisy_0', 'outputs2/test_output')

