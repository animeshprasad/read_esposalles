# -*- coding: utf-8 -*-

'''
@date: 22 déc. 2017
@author: JL Meunier
'''
import sys
import os.path
from .Alignment import Align

class Record:
    """
    a record contains:
    - an id
    - a list of list of words
    - a list of lists of 'category' classes
    - a list of list of 'person' classes
    """
    
    lCATEGORY = ["name", "surname", "occupation", "location", "state", "other"]
    lPERSON = ["wife", "husband",
               "wifes_father", "wifes_mother",
               "husbands_father", "husbands_mother",
               "other_person",
               "none"
               ]

    class_dict = {'cat':lCATEGORY, 'oth':lPERSON}

    dCATEGORY_index = {s: i for i, s in enumerate(lCATEGORY)}
    dPERSON_index = {s: i for i, s in enumerate(lPERSON)}
    # used for indexing only for jointly learning using the crossed label 
    dJOINT_index = {}
    active_dJOINT_index = {}
    for class_name1, index1 in dCATEGORY_index.items():
        for class_name2, index2 in dPERSON_index.items():
            dJOINT_index [index1*len(dPERSON_index)+index2] = class_name1+ '-' + class_name2
    
 
    def __init__(self, sID, llsWord=None, lliC=None, lliP=None):
        """
        A record contains:
        - a list of word-lines
        - a list of category-line,
        - a list of person-lines
        A X-line is a list of X.
        """
        self.sID = sID
        self.llsW = list() if llsWord is None else llsWord
        self.lliC = list() if lliC is None else lliC
        self.lliP = list() if lliP is None else lliP

    def reindexJoint(self, new_list):
        """
        input:
        new_list - set of labels to act as the index for labels.
        """
        for items in  new_list:
            if items not in self.active_dJOINT_index.keys():
                self.active_dJOINT_index[items] = len(self.active_dJOINT_index)

    def addLine(self, sLine, sC, sP):
        """
        input:
        sLine, sC, sP - corresponding tokens, cat labels, person labels 
        """
        lsW, lsC, lsP = sLine.split(), sC.split(), sP.split()
        assert len(lsW) == len(lsC)
        assert len(lsW) == len(lsP)
        self.llsW.append(lsW)
        self.lliC.append([self.dCATEGORY_index[s] for s in lsC])
        self.lliP.append([self.dPERSON_index[s] for s in lsP])

    def printLine(self):
        print (self.llsW, self.lliC, self.lliP)

    def getRecordText(self):
        return self.llsW

    def getRecordLabel(self, labels='cat'):
        """
        input:
        labels - Get relevent labels depending on the task.
                (cat|per)
        """
        return self.lliC if labels=='cat' else self.lliP

        
    @classmethod
    def load(cls, sPath):
        lRecord = list()
        
        sfnLine = os.path.join(sPath, "transcription.txt")
        sfnCate = os.path.join(sPath, "category.txt")
        sfnPers = os.path.join(sPath, "person.txt")
        with open(sfnLine) as fL,  \
             open(sfnCate) as fC,  \
             open(sfnPers) as fP:
                
            cur_Record, cur_sID, curiLine = None, None, -1
            sLine = fL.readline()
            while sLine:
                assert sLine.strip(), "Empty line in transcription.txt???"
                sCLine, sPLine = fC.readline(), fP.readline()
                iCol = sLine.index(':')
                sPage_Record_Line = sLine[:iCol]
                sPageId, sRecordId, sLineId = sPage_Record_Line.split('_')
                sID = "%s_%s" % (sPageId, sRecordId)
                iLine = int(sLineId[4:])

                if sID != cur_sID:
                    if cur_sID is not None: lRecord.append(cur_Record)
                    #create a new one!
                    cur_Record, cur_sID = Record(sID), sID
                    assert iLine == 0
                else:
                    assert curiLine+1 == iLine, (curiLine, iLine)
                curiLine = iLine
                    
                # add this line!
                assert sPage_Record_Line == sCLine[:iCol], (sID, sCLine)
                assert sPage_Record_Line == sPLine[:iCol]
                cur_Record.addLine(sLine[iCol+1:], sCLine[iCol+1:], sPLine[iCol+1:])
                
                sLine = fL.readline()
            
        if cur_sID is not None: lRecord.append(cur_Record)
             
        return lRecord


    @classmethod
    def load_flatten_with_noisy_alignment(cls, sPath):
        lRecord = list()
         
        forced_alignment = Align().align_diff_transcriptions()
        sfnLine = os.path.join(sPath, "transcription.txt")
        sfnCate = os.path.join(sPath, "category.txt")
        sfnPers = os.path.join(sPath, "person.txt")
        with open(sfnLine) as fL,  \
             open(sfnCate) as fC,  \
             open(sfnPers) as fP:

            cur_Record, cur_sID, curiLine = None, None, -1
            sLine = fL.readline()
            while sLine:
                assert sLine.strip(), "Empty line in transcription.txt???"
                sCLine, sPLine = fC.readline(), fP.readline()
                iCol = sLine.index(':')
                sPage_Record_Line = sLine[:iCol]
                sPageId, sRecordId, sLineId = sPage_Record_Line.split('_')
                sID = "%s_%s" % (sPageId, sRecordId)
                iLine = int(sLineId[4:])


                if sID != cur_sID:
                    if cur_sID is not None:
                        if cur_sID in forced_alignment.keys(): lRecord.append(cur_Record)
                    #create a new one!
                    cur_Record, cur_sID = Record(sID), sID
                    assert iLine == 0
                else:
                    assert curiLine+1 == iLine, (curiLine, iLine)
                curiLine = iLine

                # add this line!
                assert sPage_Record_Line == sCLine[:iCol], (sID, sCLine)
                assert sPage_Record_Line == sPLine[:iCol]
                cur_Record.addLine(sLine[iCol+1:], sCLine[iCol+1:], sPLine[iCol+1:])

                sLine = fL.readline()

        if cur_sID is not None:
            if cur_sID in forced_alignment.keys(): lRecord.append(cur_Record)

        return lRecord

    
    def flatten(self):
        """
        Returns a flattened list of words,
                a flattened list of category index,
                a flattened list of person index,
        """
        return ([sW for _l in self.llsW for sW in _l ],
                [iC for _l in self.lliC for iC in _l],
                [iP for _l in self.lliP for iP in _l])
        
    def __str__(self):
        sW, sC, sP = "", "", ""
        for _sW, _iC, _iP in zip(*self.flatten()):
            _sC = self.lCATEGORY[_iC]
            _sP = self.lPERSON[_iP]
            n = max(len(_sW), len(_sC), len(_sP)) + 1
            sFmt = " %%%ds" % n
            sW += sFmt % _sW
            sC += sFmt % _sC
            sP += sFmt % _sP
        return "\n".join([sW, sC, sP])
    
def test_load(capsys):    
    sPath = "../data"
    
    lRecord = Record.load(sPath)
    with capsys.disabled():
        for r in lRecord:
            print(r)
        print(len(lRecord))
