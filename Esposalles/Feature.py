# -*- coding: utf-8 -*-

"""
Feature Extracting Pipeline

@author: Animesh Prasad
@date: 9 Mar 2018
"""
import numpy as np

from sklearn.feature_extraction.text import CountVectorizer

class Feature:
    """
    This class acts as n gram to index keeper aka vocab indexing class
    The class can be initiliased using __init__ with a unique_word list containing
    list of unique tokens to be counted in the vocab.

    Alternatively, the vocab can also be initiliased togather with the data need to be 
    converted into index using fit_on_X method. However, fit_on_X canot be called before
    initlizing the vectoriser by passing the list of unique tokens. 

    fit_on_Y just indexes labels for the sake of uniformity. 
    """

    def __init__(self, unique_words=None):
        """
        input: 
        unique_words - a list of unique words. need to be set only once. 
        """
        # N-gram from 1 upto 3, keeping case showed performance particulary for NER classification
        # This parameter can be changed depending on the dataset
        self.ngram_vectorizer = CountVectorizer(analyzer='char_wb', lowercase=False,  ngram_range=(1, 3))
        self.max_features = None
        self.max_chars_length = 10   # This hyperparameter won't have any effect as it would be recompusted as max length
        self.max_tokens_length = 30  # This hyperparameter won't have any effect as it would be recompusted as max length
        if unique_words:
            self.max_features = self.ngram_vectorizer.fit_transform(unique_words)

    def get_feature_extractor(self):
        return self.ngram_vectorizer

    def set_feature_extractor(self, extractor):
        self.ngram_vectorizer = extractor
        # Just not None
        self.max_features = True
    
    def get_feature_names(self):
        if self.max_features is not None:
            return self.ngram_vectorizer.get_feature_names()
        return None

    def fit_index_on_X(self, list_of_record_texts, unique_words=None):
        """
        input:
        list_of_record_texts - list of records text, where record texts are list of tokens.
        unique_words - a list of unique words. need to be set only once. Only needed here,
                       if already not initilized earlier.

        returns:
        list of list of features per token, where features per token are indexed n grams as numpy array.
        """
        if unique_words is None and self.max_features is None:
            raise Exception('Initilaize with train features first')
        if unique_words and self.max_features is None:
            self.max_features = self.ngram_vectorizer.fit_transform(unique_words)
        x_to_return = []
        f = lambda x: [ self.ngram_vectorizer.get_feature_names().index(i) for i in x
                        if i in self.ngram_vectorizer.get_feature_names() ]
        for items in list_of_record_texts:
            k = max([len(words) for words in items])
            if k > self.max_chars_length:
                self.max_chars_length = k
            x_record = [f(words) for words in items]
            if len(x_record) > self.max_tokens_length:
                self.max_tokens_length = len(x_record)
            x_to_return.append(x_record)
        return (x_to_return, [self.max_tokens_length, self.max_chars_length])


    def fit_on_X(self, list_of_record_texts, unique_words=None):
        """
        input:
        list_of_record_texts - list of records text, where record texts are list of tokens.
        unique_words - a list of unique words. need to be set only once. Only needed here,
                       if already not initilized earlier.

        returns:
        list of list of features per token, where features per token are indexed n grams as numpy array.
        """
        if unique_words is None and self.max_features is None:
            raise Exception('Initilaize with train features first')
        if unique_words and self.max_features is None:
            self.max_features = self.ngram_vectorizer.fit_transform(unique_words)
        x_to_return = []
        for items in list_of_record_texts:
            x_record = self.ngram_vectorizer.transform(items).toarray().astype(int)
            x_to_return.append(x_record)
        return x_to_return

    def fit_Y(self, list_of_record_labels):
        """
        input:
        list_of_record_labels - list of records labels, where record texts are list of labels.
        returns:
        list of numpy array.
        """
        y_to_return = []
        for items in list_of_record_labels:
            y_record = np.asarray(items, dtype='int')
            y_to_return.append(y_record)
        return y_to_return
