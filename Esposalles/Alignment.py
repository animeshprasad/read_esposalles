# -*- coding: utf-8 -*-

'''
@date: 19 April 2019
@author: Animesh Prasad
'''

import os.path
import numpy as np
import scipy.optimize as opt


class Align:
    """
    Take two transcript objects (true and noisy) and force aligns using the Hungarian Algorithm
    returns the align object as tuple
    """
    def __init__(self, ltrue=None, lnoisy=None):
        self.ltrue = ltrue if ltrue is not None else Transcript(0).load_transcript('data', 'transcription.txt')
        self.lnoisy = lnoisy if lnoisy is not None else Transcript(0).load_transcript('data', 'bestpath.txt')
        self.align = None

        #Might be better????
        #self.ltrue, self.lnoisy = self.lnoisy, self.ltrue


    def levenshtein(seld, seq1, seq2):
        """
        Edit distance between two sequence of characters
        """  
        size_x = len(seq1) + 1
        size_y = len(seq2) + 1
        matrix = np.zeros ((size_x, size_y))
        for x in range(size_x):
            matrix [x, 0] = x
        for y in range(size_y):
            matrix [0, y] = y

        for x in range(1, size_x):
            for y in range(1, size_y):
                if seq1[x-1] == seq2[y-1]:
                    matrix [x,y] = min(
                    matrix[x-1, y] + 1,
                    matrix[x-1, y-1],
                    matrix[x, y-1] + 1
                    )
                else:
                    matrix [x,y] = min(
                    matrix[x-1,y] + 1,
                    matrix[x-1,y-1] + 1,
                    matrix[x,y-1] + 1
                    )
        return matrix


    def levenshtein_with_cer(self, cost_matrix):
        """
        Edit Distance between sequence of tokens, cost of edits are calculated 
        as cost of Edit for the characters of the token
        """
        size_x = cost_matrix.shape[0] + 1
        size_y = cost_matrix.shape[1] + 1
        matrix = np.zeros ((size_x, size_y))
        for x in range(size_x):
            matrix [x, 0] = x*0.1
        for y in range(size_y):
            matrix [0, y] = y*0.1

        for x in range(1, size_x):
            for y in range(1, size_y):
                matrix [x,y] =  min(
                matrix[x-1, y] + 0.1 + cost_matrix[x-1, y-1],
                matrix[x-1, y-1] + cost_matrix[x-1, y-1],
                matrix[x, y-1] + 0.1 + cost_matrix[x-1, y-1]
                )

        assigned = np.argmin(matrix[1:,1:], axis=1)
        true = np.array([ i for i in range(assigned.shape[0])])
        return (true, assigned)


    def get_alignment(self, rows, cols, algo=0):
        """
        Method A: get alignment at token level, with cost computed at char level 
        """
        #Lets make the cost matrix now
        cost_matrix=np.zeros((len(rows), len(cols)),dtype=float)


        for a,i in enumerate(rows):
            curRef=i
            for b,j in enumerate(cols):
                runElt=j
                cost = self.levenshtein(curRef,runElt)/(len(curRef) + len(runElt))
                cost_matrix[a,b] = cost[-1,-1]
        if algo == 0:
            row_ind, col_ind = self.levenshtein_with_cer(cost_matrix)
        else:
            row_ind, col_ind = opt.linear_sum_assignment(cost_matrix)
        return [(rows[i],cols[i]) for i in range(min(len(rows), len(cols)))]


    def get_alignment_char_level(self, rows, cols):
        """
        Method B: get alignment at char level and retrofit at token level
        """
        len_rows = [len(_i) for _i in rows]
        dict_rows = {}
        dict_cols={}
        dict_rows[0] = len_rows[0]
        for _i in range(1, len(len_rows)):
            dict_rows[_i] =  dict_rows[_i-1] + len_rows[_i]

        len_cols = [len(_i) for _i in cols]
        dict_cols[0] = len_cols[0]
        dict_cols_list = [len_cols[0]]

        for _i in range(1, len(len_cols)):
            dict_cols[_i] =  dict_cols[_i-1] + len_cols[_i]
            dict_cols_list.append(dict_cols[_i-1] + len_cols[_i])

        to_return_row = []
        to_return_col = []        


        rows_char = ''.join(rows)
        cols_char = ''.join(cols)
        cost = self.levenshtein(rows_char, cols_char)

        for token_index, char_index in dict_rows.items():
            to_return_row.append(token_index)
            to_return_col.append( np.argmin(cost[char_index][dict_cols_list])) 
        
        assert(len(to_return_row) == len(to_return_col))
        return [(rows[to_return_row[i]], cols[to_return_col[i]]) for i in range(len(to_return_row))] 


    def align_diff_transcriptions(self):
        """
        Align the transcriptions using one of the methods described above
        """
        true_index, true_text = self.ltrue
        noisy_index, noisy_text = self.lnoisy

        _true_text = [i.flatten() for i in true_text]
        _noisy_text = [i.flatten() for i in noisy_text]

        if len(true_index) != len(noisy_index):
            print("Some Records missing either True or HTR alignments!!! Will be ignored.")

        a={}
        count = 0
        skip_thresh = 5
        for items in set(true_index).intersection(set(noisy_index)):
            out_align = self.get_alignment_char_level(_true_text[true_index.index(items)], _noisy_text[noisy_index.index(items)])
            
            if skip_thresh is not None:
                na_count = 0
                for i in range(len(out_align)):
                    if out_align[i][0] != out_align[i][1]:
                        #HTR-Error Here
                        na_count += 1
                count += na_count
                if (na_count > skip_thresh):
                    continue
                a[items] = out_align
            else:
                a[items] = out_align

        print(str(count) + ' Token level miss-alignments found!')

        def unflatten(list_b, list_a):
            index_a = [len(l1) for l1 in list_a.llsW]
            assert(len(list_b) == sum(index_a))
            list_c = []
            s = 0
            for  index in index_a:
                # Returning only noisy
                list_c.append([x[1] for x in list_b[s:s+index]])
                s += index
            return list_c

        to_return = {}
        for ids, items in zip(true_index, true_text):
            if ids in a.keys():
                to_return[ids] = unflatten(a[ids], items)
               
        self.align = to_return
        return self.align


class Transcript:
    """
    Read the text in text objects, can be used for reading the blind test data 
    """
 
    def __init__(self, sID, llsWord=None):
        self.sID = sID
        self.llsW = list() if llsWord is None else llsWord

    def addLine(self, sLine):
        lsW = sLine.split()
        self.llsW.append(lsW)

    def printLine(self):
        print (self.llsW)

    def flatten(self):
        """
        Returns a flattened list of words
        """
        return ([sW for _l in self.llsW for sW in _l ])

    @classmethod
    def load_transcript(cls, sPath, transcript_type='transcription.txt'):
        lTranscript = list()
        lsID = list()
       
        sfnLine = os.path.join(sPath, transcript_type)
        with open(sfnLine) as fL:
            cur_Record, cur_sID, curiLine = None, None, -1
            sLine = fL.readline()
            while sLine:
                assert sLine.strip(), "Empty line in transcription???"
                iCol = sLine.index(':')
                sPage_Record_Line = sLine[:iCol]
                sPageId, sRecordId, sLineId = sPage_Record_Line.split('_')
                sID = "%s_%s" % (sPageId, sRecordId)
                iLine = int(sLineId[4:])

                if sID != cur_sID:
                    if cur_sID is not None:
                        lTranscript.append(cur_Record)
                        lsID.append(cur_sID)
                    #create a new one!
                    cur_Record, cur_sID = Transcript(sID), sID
                    assert iLine == 0
                else:
                    assert curiLine+1 == iLine, (curiLine, iLine)
                curiLine = iLine
                cur_Record.addLine(sLine[iCol+1:])
                sLine = fL.readline()
            
        if cur_sID is not None:
            lTranscript.append(cur_Record)
            lsID.append(cur_sID)

        assert (len(lTranscript) == len(lsID))
        return (lsID, lTranscript)
