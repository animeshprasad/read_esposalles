# -*- coding: utf-8 -*-

"""
Starting point of the code which calls modules for feature extraction, 
trains the specified model with the data and calls the module to generate
the error analysis.

This file also has a counter-part (neural_train.py) for crf models using keras. 

@author Animesh Prasad
@data March 2018
"""
import numpy as np
import pickle as p

from utils import make_node_features, make_links_for_graph, add_features_to_links
from utils import do_label_product, do_label_linearisation, open_tuples, make_tuples
from Esposalles.Record import Record

from pystruct.models import ChainCRF, GraphCRF, EdgeFeatureGraphCRF, NodeTypeEdgeFeatureGraphCRF
from pystruct.learners import OneSlackSSVM

from pystruct.utils import SaveLogger


MAX_ITER = 200
C = .1
FOLDS = 10
N_JOBS = 4
VERBOSE = 0
SHOW_LOSS_EVERY = 20


#def plot_model_loss(path):
    #ssvm = FrankWolfeSSVM(model=model, C=C, max_iter=20, verbose=verbose, n_jobs=1, logger=SaveLogger('models/CCRF'), show_loss_every=4)
    #ssvm =  SaveLogger('models/CCRF').load()
    #import matplotlib as mpl 
    #mpl.use('GTKAgg')
    #import matplotlib.pyplot as plt
    #plt.switch_backend('agg')
    #fig = plt.figure()
    #fig.canvas.set_window_title("dir=%s  model=%s  "%('models', 'CCRF'))
    #plt.plot(ssvm.objective_curve_)
    #plt.plot(ssvm.loss_curve_)
    #plt.xlabel("Iteration / 10")
    #plt.ylabel("Loss")
    #plt.show()


def train(model_id=-1):

    def augment_data(x, xN):
        a = []
        for item in [x, xN]:
            for i in item:
                a.append(np.array(i))
        return np.array(a)


    for label_type in ['cat', 'oth']:
        for folds in range (FOLDS):
            x_tr, x_v, x_t, y_tr, y_v, y_t, feature_extractor = make_node_features(label_type, folds)
            xN_tr, xN_v, xN_t, yN_tr, yN_v, yN_t, feature_extractor = make_node_features(label_type, folds, feature_extractor, True)
            
            def augment_data(x, xN):
                a = []
                for item in [x, xN]:
                    for i in item:
                        a.append(np.array(i))
                return np.array(a)

            train_set = {
                         'true' : (x_tr, y_tr),
                         'noisy' : (xN_tr, yN_tr),
                         'mix' : (augment_data(x_tr , xN_tr), augment_data(y_tr , yN_tr)) 
                        }           

            test_set = {
                        'true' : (x_t, y_t),
                        'noisy' : (xN_t, yN_t)
                       } 

            if model_id == -1 or model_id == 0:
                for train_name, (x_tr, y_tr) in train_set.items():
                    model = ChainCRF()
                    ssvm = OneSlackSSVM(model=model, C=C, max_iter=MAX_ITER, verbose=VERBOSE, n_jobs=N_JOBS, show_loss_every=SHOW_LOSS_EVERY)
                    ssvm.fit(x_tr, y_tr)
                    for test_name, (x_t, y_t) in test_set.items():
                        y_pr = ssvm.predict(x_t)
                        with open('outputs/' + 'CCRF_' + label_type + '_' +  train_name + 'ON' + test_name + '_'  + str(folds), 'wb') as fp:
                            p.dump((y_t, y_pr), fp)
                       

            if model_id == -1 or model_id == 1:
                for train_name, (x_tr, y_tr) in train_set.items():
                    model = GraphCRF(directed=True)
                    # Directed was found to show better performance, maybe just because of 2*params as non directed 
                    ssvm = OneSlackSSVM(model=model, C=C, verbose=VERBOSE, max_iter=MAX_ITER, show_loss_every=SHOW_LOSS_EVERY, n_jobs=N_JOBS)
                    for link_type in ['skipped']:
                        skip=[1, 2, 3, 4, 5]
                        X_tr = make_links_for_graph(x_tr, label_type, link_type, skip)
                        ssvm.fit(X_tr, y_tr)
                        for test_name, (x_t, y_t) in test_set.items():
                            X_t = make_links_for_graph(x_t, label_type,  link_type, skip)
                            y_pr = ssvm.predict(X_t)
                            with open('outputs/' + 'GCRF_' + label_type + '_' +  train_name + 'ON' + test_name + '_'  + str(folds), 'wb') as fp:
                                p.dump((y_t, y_pr), fp)


            if model_id == -1 or model_id == 2:
                for train_name, (x_tr, y_tr) in train_set.items():
                    # TODO: Experiment with the normalised_skipped 
                    model = EdgeFeatureGraphCRF()
                    ssvm = OneSlackSSVM(model=model, C=C, verbose=VERBOSE, max_iter=MAX_ITER, show_loss_every=SHOW_LOSS_EVERY, n_jobs=N_JOBS)
                    for link_type in ['skipped']:
                        skip=[1, 2, 3, 4, 5]
                        X_tr = make_links_for_graph(x_tr, label_type, link_type, skip)
                        X_tr = add_features_to_links(X_tr, skip=max(skip)+1)
                        ssvm.fit(X_tr, y_tr)
                        for test_name, (x_t, y_t) in test_set.items():
                            X_t = make_links_for_graph(x_t, label_type, link_type, skip)
                            X_t = add_features_to_links(X_t, skip=max(skip)+1)
                            y_pr = ssvm.predict(X_t)
                            with open('outputs/' + 'EFGCRF_' + label_type + '_' +  train_name + 'ON' + test_name + '_'  + str(folds), 'wb') as fp:
                                p.dump((y_t, y_pr), fp)


    
    for folds in range (FOLDS):
        # This Record object will store the re-mapping of the crossed label,
        # Other way is to generate and re-map all the labels in Record object
        # but that can be assumed to be 'cheating' if some combination is not
        # cross combination is not seen in training 
        # TODO: Verify this need 

        x1_tr, x1_v, x1_t, y1_tr, y1_v, y1_t, feature_extractor = make_node_features('cat', folds)
        _, _, _, y2_tr, y2_v, y2_t, _ = make_node_features('oth', folds)
        xN1_tr, xN1_v, xN1_t, yN1_tr, yN1_v, yN1_t, feature_extractor = make_node_features('cat', folds, feature_extractor, True)
        _, _, _, yN2_tr, yN2_v, yN2_t, _ = make_node_features('oth', folds, feature_extractor, True)


        train_set = {
                     'true' : (x1_tr, y1_tr, y2_tr),
                     'noisy' : (xN1_tr, yN1_tr, yN2_tr),
                     'mix' : (augment_data(x1_tr , xN1_tr), augment_data(y1_tr , yN1_tr), augment_data(y2_tr, yN2_tr))
                    }

        test_set = {
                    'true' : (x1_t, y1_t, y2_t),
                    'noisy' : (xN1_t, yN1_t, yN2_t)
                   }

        label_type =  'joint'
        for train_name, (x1_tr, y1_tr, y2_tr) in train_set.items(): 
            temp=Record(0)
            y_joint_tr = do_label_product(y1_tr, y2_tr, temp)
            for test_name, (x1_t, y1_t, y2_t) in test_set.items():
                y_joint_t = do_label_product(y1_t, y2_t, temp)

                if model_id == -1 or model_id == 3.0:
                    model = ChainCRF(n_states=len(temp.active_dJOINT_index))
                    ssvm = OneSlackSSVM(model=model, C=C, max_iter=MAX_ITER, verbose=VERBOSE, n_jobs=N_JOBS, show_loss_every=SHOW_LOSS_EVERY)
                    try:
                        ssvm.fit(x1_tr, y_joint_tr)
                        y_pr = ssvm.predict(x1_t)
                        with open('outputs/' + 'CCRF_' + label_type + '_' +  train_name + 'ON' + test_name + '_'  + str(folds), 'wb') as fp:
                            p.dump((y_joint_t, y_pr), fp)
                    except ValueError:
                        print('This fold is not trained-evaluated because because of 0 training sample for a class')


                if model_id == -1 or model_id == 3.1:
                    model = GraphCRF(n_states=len(temp.active_dJOINT_index), directed=True)
                    # Directed was found to show better performance, maybe just because of 2*params as non directed 
                    ssvm = OneSlackSSVM(model=model, C=C, verbose=VERBOSE, max_iter=MAX_ITER, show_loss_every=SHOW_LOSS_EVERY, n_jobs=N_JOBS)
                    for link_type in ['skipped']:
                        skip=[1, 2, 3, 4, 5]
                        X_tr = make_links_for_graph(x1_tr, label_type, link_type, skip)
                        X_t = make_links_for_graph(x1_t, label_type,  link_type, skip)
                        try:
                            ssvm.fit(X_tr, y_joint_tr)
                            y_pr = ssvm.predict(X_t)
                            with open('outputs/' + 'GCRF_' + label_type + '_' +  train_name + 'ON' + test_name + '_'  + str(folds), 'wb') as fp:
                                p.dump((y_joint_t, y_pr), fp)
                        except ValueError:
                            print('This fold is not trained-evaluated because because of 0 training sample for a class')



                if model_id == -1 or model_id == 3.2:
                    model = EdgeFeatureGraphCRF(n_states=len(temp.active_dJOINT_index))
                    ssvm = OneSlackSSVM(model=model, C=C, verbose=VERBOSE, max_iter=MAX_ITER, n_jobs=N_JOBS, show_loss_every=SHOW_LOSS_EVERY)
                    for link_type in ['skipped']:
                        skip=[1, 2, 3, 4, 5]
                        X_tr = make_links_for_graph(x1_tr, label_type, link_type, skip)
                        X_t = make_links_for_graph(x1_t, label_type, link_type, skip)
                        X_tr = add_features_to_links(X_tr, skip=max(skip)+1)
                        X_t = add_features_to_links(X_t, skip=max(skip)+1)
                        try:
                            ssvm.fit(X_tr, y_joint_tr)
                            y_pr=ssvm.predict(X_t)
                            with open('outputs/' + 'EFGCRF_' + label_type + '_' +  train_name + 'ON' + test_name + '_'  + str(folds), 'wb') as fp:
                                p.dump((y_joint_t, y_pr), fp)
                        except ValueError: 
                            print('This fold is not trained-evaluated because because of 0 training sample for a class')



        label_type =  'cat' #useless
        if model_id == -1 or model_id == 4:
            for train_name, (x1_tr, y1_tr, y2_tr) in train_set.items():
                temp=Record(0)
                y_tr = do_label_linearisation(label_type, y1_tr, y2_tr, temp)
                for test_name, (x1_t, y1_t, y2_t) in test_set.items():
                    y_t = do_label_linearisation(label_type, y1_t, y2_t, temp)
                    for link_type in ['skipped']:
                        # This set of features and link represent self edge links

                        # We create self links by assuming both the tasks independently 
                        # and trating them as  edge feature CRF. Since we are using same feature for both
                        # the types we dont need to repeat this 2 times (once both node types)
                        skip = [1, 2, 3, 4, 5]
                        edge_feature_length =  max(skip) + 1
                        X_tr = make_links_for_graph(x1_tr, label_type, link_type, skip)
                        X_t = make_links_for_graph(x1_t, label_type, link_type, skip)
                        X_tr = open_tuples(add_features_to_links(X_tr, edge_feature_length))
                        X_t = open_tuples(add_features_to_links(X_t, edge_feature_length))
                        X_tr_nf = [X_tr[0], X_tr[0]]
                        X_t_nf = [X_t[0], X_t[0]]
                        skip = [0]
                        # This set of features and link represent self edge links
                        # These features wouldn't be used at all and in fact are same the orignal node type features
                        # However now we interpret the links and link features as cross_link and cross link_features
                        # In fact this is just an easy way to produce link and link_features 
                        Xcross_tr = make_links_for_graph(x1_tr, label_type, link_type, skip)
                        Xcross_t = make_links_for_graph(x1_t, label_type, link_type, skip)
                        Xcross_tr = open_tuples(add_features_to_links(Xcross_tr, edge_feature_length))
                        Xcross_t = open_tuples(add_features_to_links(Xcross_t, edge_feature_length))

                        # These are the functions which will be used to interpret or customise the cross_links 
                        # to act as our requirement
                        # TODO: Other link options
                        def links(e00, e01, e10=None, e11=None, repeat_links=True):
                            if repeat_links:
                                return [e00, e01, e01, e00]
                            else:
                                return [e00, e01, e10, e11]
                    
                        def link_features(ef00, ef01, ef10=None, ef11=None, repeat_link_features=True):
                            if repeat_link_features:
                                return [ef00, ef01, ef01, ef00]
                            else:
                                return [ef00, ef01, ef10, ef11]

                        X_tr_e = links(X_tr[1], Xcross_tr[1])
                        X_t_e = links(X_t[1], Xcross_t[1])
                        X_tr_ef = link_features(X_tr[2], Xcross_tr[2])
                        X_t_ef = link_features(X_t[2], Xcross_t[2])
                        #Rearrange the arrays in the format as required the by the model
                        #i.e. a list of tuples
                        X_tuple_tr = make_tuples(X_tr_nf, X_tr_e, X_tr_ef, 2)
                        X_tuple_t = make_tuples(X_t_nf, X_t_e, X_t_ef, 2)
                        model = NodeTypeEdgeFeatureGraphCRF(2, 
                                  [len(temp.class_dict['cat']), len(temp.class_dict['oth'])],
                                  [X_tr[0][0].shape[1], X_tr[0][0].shape[1]],
                                  [[edge_feature_length, edge_feature_length],
                                      [edge_feature_length, edge_feature_length]],
                                  inference_method="ad3+",
                                  l_class_weight=None)
                        ssvm = OneSlackSSVM(model=model, C=C, verbose=VERBOSE, max_iter=MAX_ITER, n_jobs=N_JOBS, show_loss_every=SHOW_LOSS_EVERY)
                        ssvm.fit(X_tuple_tr, y_tr)
                        y_pr=ssvm.predict(X_tuple_t)
                        with open('outputs/' + 'NTEFGCRF_' + label_type + '_' +  train_name + 'ON' + test_name + '_'  + str(folds), 'wb') as fp:
                            p.dump((y_t, y_pr), fp)



if __name__ == "__main__":
    train(-1)

