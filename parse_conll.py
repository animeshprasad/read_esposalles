from Esposalles.Feature import Feature
import numpy as np

def load_data(ngram=True):
    train = _parse_data(open('data_conll/eng.train'))
    test = _parse_data(open('data_conll/eng.testa'))

    vocab = sorted(set([w[0]  for l in train for w in l]))
    tags = sorted(list(set(row[3] for sample in train + test for row in sample)))  # in alphabetic order


    all_features = _process_data(train, test, vocab,  tags, ngram)
    return all_features


def _parse_data(fh):
    string = fh.read()
    data = [[row.split() for row in sample.split('\n')] for sample in string.strip().split('\n\n')]
    fh.close()
    return data


def _process_data(train, test, vocab, tags, ngram, extractor=None):
    train_data = [ [w[0]  for w in l ] for l in train]
    test_data = [ [w[0]  for w in l ] for l in test]

    features = Feature()
    if extractor:
        features.set_feature_extractor(extractor)

    if ngram:
        x_tr = np.array(features.fit_on_X(train_data, vocab))
        x_t = np.array(features.fit_on_X(test_data))
        y_tr = [np.array([tags.index(w[3]) for w in l]) for l in train]
        y_t = [np.array([tags.index(w[3]) for w in l]) for l in test]
        #print (len(y_tr), len(y_t))
        
        return x_tr, None, x_t, y_tr, None, y_t, features.get_feature_extractor(), len(tags)

    else:
        x_tr, list_max_tr = features.fit_index_on_X(train_data, vocab)
        x_t, list_max_t = features.fit_index_on_X(test_data)
        y_tr = [np.array([tags.index(w[3]) for w in l]) for l in train]
        y_t = [np.array([tags.index(w[3]) for w in l]) for l in test]
        list_max = [max([a,b]) for a,b in zip(list_max_tr, list_max_t) ]
        
        return x_tr, None, x_t, y_tr, None, y_t, features.get_feature_extractor(), list_max, len(tags)

if __name__ == "__main__":
    load_data()
