# READ-Esposalles
Our exploration of the Esposalles task.

Basic Workflow:
python make_splits.py
python train.py

Complete Directory Structure:

-data          ....... orignal labeled data
--category.py
--person.txt
--transcription.txt


-Esposalles   ........ class files
--Feature.py
--Record.py


-make_splits.py   ......... create n fold by splitting and rearrnging in pickle files 
-partition$i.py   ......... the folds, id represents the split used for test

-train.py        ......... model traning and testing happens here

-utils.py   ......... interacts with pickle files to get features for training and other data related utilities 

---------------------------------------------------------------------------------------------------------------

Running the code:

python make_splits.py 
#Expected output 10 (tunable parameter  in the code) pickle files representing the 10 folds
#The pickle files have train, val and test splits.


python train.py
#Runs the best model (found during the dev experiments)
#All valid experiements can be run by changing parametrs in the train file
---------------------------------------------------------------------------------------------------------------

Full documentation at http://confluence.int.europe.naverlabs.com/display/REP/Esposalles

