# -*- coding: utf-8 -*-

"""
Wrapper over the keras contaning all the relevant models with appropriate
configurations as required for our task.

@author Animesh Prasad
@data April 2018
"""

from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import numpy as np
from collections import Counter

from keras.models import Sequential, Model
from keras.layers import Masking, Reshape, TimeDistributed, Input, Dense, Embedding, Bidirectional, LSTM, Conv1D, GlobalMaxPooling1D
from keras.preprocessing.sequence import pad_sequences
from keras_contrib.layers import CRF
from keras import backend as K
from keras.layers.core import Lambda

EPOCHS = 10
EMBED_DIM = 20
BiRNN_UNITS = 20
EMBED_PATH = '../word2vec/data/vectorsnew.bin'

def load_pretrained_embeddings(vocab, size, emb_path):
    to_return = np.random.uniform(low=0.001, high=0.0001, size=(len(vocab), size))
    with open(emb_path, 'r') as embed_file:
        for line in embed_file:
            tokens = line.strip().split()
            if len(tokens) == size+1:
                try:
                    to_return[vocab.index(tokens[0])]= np.array([float(items) for items in tokens[1:]], dtype=np.float32)
                except:
                    pass
    return to_return


def crf(vocab, class_labels, train_x, train_y): 
    # --------------
    # 1. Regular CRF
    # --------------
    print('==== training CRF ====')
    model = Sequential()
    crf = CRF(class_labels, sparse_target=True, batch_input_shape=(1, None, train_x[0].shape[1]))
    model.add(crf)
    model.summary()
    model.compile('adam', loss=crf.loss_function, metrics=[crf.accuracy])
    for  epoch in range(EPOCHS):
        for train_x_sample, train_y_sample in zip(train_x, train_y):
            model.fit(np.array([train_x_sample]), np.array([train_y_sample]), 
                      epochs=1, batch_size=1, verbose=0)
            # Here?
            #model.reset_states()
    return model


def blstm_crf(vocab, class_labels, train_x, train_y):
    # -------------
    # 2. BiLSTM-CRF
    # -------------
    print('==== training BiLSTM-CRF ====')
    model = Sequential()
    model.add(Bidirectional(
                            LSTM(BiRNN_UNITS // 2, return_sequences=True, stateful=True,
                                 dropout=0.2, recurrent_dropout=0.2),
                            batch_input_shape=(1, None, train_x[0].shape[1])
                          ))
    crf = CRF(class_labels, sparse_target=True)
    model.add(crf)
    model.summary()
    model.compile('adam', loss=crf.loss_function, metrics=[crf.accuracy])
    for  epoch in range(EPOCHS):
        for train_x_sample, train_y_sample in zip(train_x, train_y):
            model.fit(np.array([train_x_sample]), np.array([train_y_sample]),
                      epochs=1, batch_size=1, verbose=0)
            # Here?
            #model.reset_states()
    return model


def blstm(vocab, class_labels, train_x, train_y):
    # -------------
    # 3. BiLSTM
    # -------------
    print('==== training BiLSTM ====')
    model = Sequential()
    model.add(Bidirectional(
                            LSTM(BiRNN_UNITS // 2, return_sequences=True, stateful=True,
                                 dropout=0.2, recurrent_dropout=0.2),
                            batch_input_shape=(1, None, train_x[0].shape[1])
                          ))
    crf = TimeDistributed(Dense(class_labels, activation='softmax'))
    model.add(crf)
    model.summary()
    model.compile('adam', loss='categorical_crossentropy', metrics=['acc'])
    for  epoch in range(EPOCHS):
        for train_x_sample, train_y_sample in zip(train_x, train_y):
            model.fit(np.array([train_x_sample]), np.array([train_y_sample]),
                      epochs=1, batch_size=1, verbose=0)
            # Here?
            #model.reset_states()
    return model


def crf_we(vocab, class_labels, train_x, train_y, use_cnn=False):
    # --------------
    # 4. Regular CRF with WE
    # --------------
    print('==== training CRF with WE ====')
    ishape = np.array(train_x).shape
    in_x = Input(batch_shape=(None, ishape[1], ishape[2]))
    x = Lambda(lambda x: K.reshape(x, (-1, ishape[2])))(in_x)
    if EMBED_PATH:
        embedding_matrix = load_pretrained_embeddings(vocab, EMBED_DIM, EMBED_PATH)
        x = Embedding(len(vocab), EMBED_DIM, mask_zero=True, weights=[embedding_matrix], trainable=False)(x)
    else:
        x = Embedding(len(vocab), EMBED_DIM, mask_zero=True)(x)
    if use_cnn:
        x = Conv1D(20,
                 3,
                 padding='valid',
                 activation='relu',
                 strides=1)(x)
        x = GlobalMaxPooling1D()(x)
    else:
        x = Bidirectional(
                      LSTM(BiRNN_UNITS // 2, return_sequences=False,
                           dropout=0.2, recurrent_dropout=0.2)
                     )(x)
    x = Lambda(lambda x: K.reshape(x, (-1, ishape[1], BiRNN_UNITS)))(x)
    crf = CRF(class_labels, sparse_target=True)
    out_y = crf(x)
    model =  Model(inputs=in_x, outputs=out_y)
    model.summary()
    model.compile('adam', loss=crf.loss_function, metrics=[crf.accuracy])
    model.fit(np.array(train_x), np.array(train_y), epochs=EPOCHS, verbose=1)
    return model


def blstm_crf_we(vocab, class_labels, train_x, train_y, use_cnn=False):
    # -------------
    # 5. BiLSTM-CRF with WE
    # -------------
    print('==== training BiLSTM-CRF with WE ====')
    ishape = np.array(train_x).shape
    in_x = Input(batch_shape=(None, ishape[1], ishape[2]))
    x = Lambda(lambda x: K.reshape(x, (-1, ishape[2])))(in_x)
    if EMBED_PATH:
        embedding_matrix = load_pretrained_embeddings(vocab, EMBED_DIM, EMBED_PATH)
        x = Embedding(len(vocab), EMBED_DIM, mask_zero=True, weights=[embedding_matrix], trainable=False)(x)
    else:
        x = Embedding(len(vocab), EMBED_DIM, mask_zero=True)(x)
    if use_cnn:
        x = Conv1D(20,
                 3,
                 padding='valid',
                 activation='relu',
                 strides=1)(x)
        x = GlobalMaxPooling1D()(x)
    else:
        x = Bidirectional(
                      LSTM(BiRNN_UNITS // 2, return_sequences=False,
                           dropout=0.2, recurrent_dropout=0.2)
                     )(x)
    x = Lambda(lambda x: K.reshape(x, (-1, ishape[1], BiRNN_UNITS)))(x)
    x = Bidirectional(
                            LSTM(BiRNN_UNITS // 2, return_sequences=True,
                                 dropout=0.2, recurrent_dropout=0.2)
                          )(x)
    crf = CRF(class_labels, sparse_target=True)
    out_y = crf(x)
    model =  Model(inputs=in_x, outputs=out_y)
    model.summary()
    model.compile('adam', loss=crf.loss_function, metrics=[crf.accuracy])
    model.fit(np.array(train_x), np.array(train_y), epochs=EPOCHS, verbose=1)
    return model


def blstm_we(vocab, class_labels, train_x, train_y, use_cnn=False):
    # -------------
    # 6. BiLSTM with WE
    # -------------
    print('==== training BiLSTM with WE ====')
    ishape = np.array(train_x).shape
    in_x = Input(batch_shape=(None, ishape[1], ishape[2]))
    x = Lambda(lambda x: K.reshape(x, (-1, ishape[2])))(in_x)
    if EMBED_PATH:
        embedding_matrix = load_pretrained_embeddings(vocab, EMBED_DIM, EMBED_PATH)
        x = Embedding(len(vocab), EMBED_DIM, mask_zero=True, weights=[embedding_matrix], trainable=False)(x)
    else:
        x = Embedding(len(vocab), EMBED_DIM, mask_zero=True)(x)
    if use_cnn:
        x = Conv1D(20,
                 3,
                 padding='valid',
                 activation='relu',
                 strides=1)(x)
        x = GlobalMaxPooling1D()(x)
    else:
        x = Bidirectional(
                      LSTM(BiRNN_UNITS // 2, return_sequences=False,
                           dropout=0.2, recurrent_dropout=0.2)
                     )(x)
    x = Lambda(lambda x: K.reshape(x, (-1, ishape[1], BiRNN_UNITS)))(x)
    x = Bidirectional(
                            LSTM(BiRNN_UNITS // 2, return_sequences=True,
                                 dropout=0.2, recurrent_dropout=0.2)
                          )(x)
    out_y = TimeDistributed(Dense(class_labels, activation='softmax'))(x)
    model =  Model(inputs=in_x, outputs=out_y)
    model.summary()
    model.compile('adam', loss='categorical_crossentropy', metrics=['acc'])
    model.fit(np.array(train_x), np.array(train_y), epochs=EPOCHS, verbose=1)
    return model


# CRF layer doesn't support multitasking? So can be implemented by using the joint labels
# as used in the feature based pystrct models, but since CRF, LSTM-CRF models are not working
# skipping the multitask experiment (In case, using joint labels model 1-6 will be used)

def blstm_multitask(vocab, class_labels1, class_labels2, train_x, train_y1, train_y2):
    # -------------
    # 7. BiLSTM Multitask
    # -------------
    print('==== training BiLSTM Multitask====')

    in_x = Input(batch_shape=(1, None, train_x[0].shape[1]))
    x = Bidirectional(
                      LSTM(BiRNN_UNITS//2, return_sequences=True, stateful=True,
                           dropout=0.2, recurrent_dropout=0.2),
                      batch_input_shape=(1, None, train_x[0].shape[1])
                     )(in_x)
    out1 = TimeDistributed(Dense(class_labels1, activation='softmax'))
    x1 = out1(x)
    out2 = TimeDistributed(Dense(class_labels2, activation='softmax'))
    x2 = out2(x)
    model =  Model(inputs=in_x, outputs=[x1, x2])
    model.summary()
    model.compile('adam', loss='categorical_crossentropy', metrics=['acc'])
    for  epoch in range(EPOCHS):
        for train_x_sample, train_y1_sample, train_y2_sample in zip(train_x, train_y1, train_y2):
            model.fit(np.array([train_x_sample]), [np.array([train_y1_sample]), np.array([train_y2_sample])],
                      epochs=1, batch_size=1, verbose=0)
            # Here?
            #model.reset_states()
    return model


def predict_multitask(model, test_x):
    test_y1_pred = []
    test_y2_pred = []
    for test_x_sample in test_x:
        pred  =  model.predict(np.array([test_x_sample]))
        test_y1_pred.append(pred[0][0])
        test_y2_pred.append(pred[1][0])
    return (test_y1_pred, test_y2_pred)

def predict(model, test_x):
    test_y_pred = []
    for test_x_sample in test_x:
        test_y_pred.append(model.predict(np.array([test_x_sample]))[0])
    return test_y_pred
