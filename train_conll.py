# -*- coding: utf-8 -*-

"""
End to end code for running the best model (found during develpment)

@author Animesh Prasad
@data May 2018
"""
import sys
import numpy as np
import pickle as p

from model_neural import  blstm, blstm_crf, predict, blstm_multitask, blstm_we, blstm_crf_we, predict_multitask
from parse_conll import load_data
from do_eval import calculate_metrics 


def discard_padding(y_pr, y_t):
    list_to_return = []
    for items_pred, items_true in zip(y_pr, y_t):
        list_to_return.append(items_pred[:len(items_true)]) 
    return list_to_return

def transform_to_onehot(y_tr, class_length):
    list_to_return = []
    for items in y_tr:
        b =  np.zeros((items.shape[0], class_length))
        b[np.arange(items.shape[0]), items] = 1
        list_to_return.append(b)
    return list_to_return

def transform_from_onehot(y_p, class_length):
    list_to_return = []
    for items in y_p:
        a = np.array([np.where(r==max(r))[0][0] for r in items])
        list_to_return.append(a)
    return list_to_return

def do_padding(unpadded_list, depth=2, max_list_per_depth=[30, 10]):
    sample_lists = []
    for samples in unpadded_list:
        token_feature_list = []
        if depth == 2: 
            for tokens in samples:
                l = np.zeros(max_list_per_depth[1], dtype=np.int32)
                l[:len(tokens)] = np.array(tokens)
                token_feature_list.append(l)
            sample_lists.append(np.array(token_feature_list +  [np.zeros(max_list_per_depth[1], dtype=np.int32) for i in range(max_list_per_depth[0] - len(token_feature_list))]))
        elif depth == 1:
            l = np.zeros(max_list_per_depth[0], dtype=np.int32)
            l[:len(samples)] = np.array(samples)
            sample_lists.append(l)
    return np.array(sample_lists)


def train(model_id=-1, times=0.05):

    if model_id == -1 or model_id == 0:
        x_tr, x_v, x_t, y_tr, y_v, y_t, feature_extractor, class_length = load_data()
        x_tr, y_tr = x_tr[0:int(14986*times)], y_tr[0:int(700*times)]

        y_tr = transform_to_onehot(y_tr, class_length)
        for model_name_str, model_name in {'blstm':blstm, 'blstm_crf':blstm_crf }.items():
            model = model_name(feature_extractor.get_feature_names(), class_length, x_tr, y_tr)
            y_pr = predict(model, x_t)
            y_pr = transform_from_onehot(y_pr, class_length)
            calculate_metrics(y_t, y_pr, average='micro')

    if model_id == -1 or model_id == 1:
        x_tr, x_v, x_t, y_tr, y_v, y_t, feature_extractor, max_list_per_depth, class_length = load_data(ngram=False)
        x_tr,  y_tr = x_tr[0:int(700*times)], y_tr[0:int(700*times)]

        x_tr = do_padding(x_tr, 2, max_list_per_depth)
        y_tr = do_padding(y_tr, 1, max_list_per_depth)
        y_tr = transform_to_onehot(y_tr, class_length)
        for model_name_str, model_name in {'blstm_we':blstm_we ,'blstm_crf_we':blstm_crf_we }.items():
            model = model_name(feature_extractor.get_feature_names(), class_length, x_tr, y_tr)
            x_t = do_padding(x_t, 2, max_list_per_depth)
            y_pr = predict(model, x_t)
            y_pr = transform_from_onehot(y_pr, class_length)
            y_pr = discard_padding(y_pr, y_t)
            calculate_metrics(y_t, y_pr, average='micro')


    if model_id == 2:
        x1_tr, x1_v, x1_t, y1_tr, y1_v, y1_t, feature_extractor, class_length1 = load_data()
        _, _, _, y2_tr, y2_v, y2_t, _, class_length2 = load_data(feature_extractor=feature_extractor)

        y1_tr = transform_to_onehot(y1_tr, class_length1)
        y2_tr = transform_to_onehot(y2_tr, class_length2)
        for test_name, (x1_t, y1_t, y2_t) in test_set.items():
            model = blstm_multitask(feature_extractor.get_feature_names(), class_length1, class_length2, x1_tr, y1_tr, y2_tr)
            y1_pr, y2_pr = predict_multitask(model, x1_t)
            y1_pr = transform_from_onehot(y1_pr, class_length1)
            y2_pr = transform_from_onehot(y2_pr, class_length2)
            calculate_metrics(y1_t, y1_pr, average='micro')
            calculate_metrics(y2_t, y2_pr, average='micro')



if __name__ == "__main__":
    if len(sys.argv) == 2:
        train(1, float(sys.argv[1]))
    else:
        train()

