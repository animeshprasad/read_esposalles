# -*- coding: utf-8 -*-

"""
Make n splits of the data
Use the n splits to make n pickles in round robin way.
Each pickle has 8/n 1/n 1/n partitioned data as train val and test.

@author JL Meunier
@author Animesh Prasad
@data March 2018
"""
import os
import pickle
import errno
from functools import reduce

from Esposalles.Record import Record

sPath = "data"
number_of_folds = 10
oPath = "data_folds/"
base_filename = "partitionN"
#Either partitionN or partition 

if not os.path.exists(os.path.dirname(oPath)):
    try:
        os.makedirs(os.path.dirname(oPath))
    except OSError as exc: # Guard against race condition
        if exc.errno != errno.EEXIST:
            raise

sPickleFile = oPath + base_filename + "0.pkl"
if os.path.exists(sPickleFile):
    raise Exception("File already exists : %s" % sPickleFile)

lRecord = None
if base_filename[-1] != 'N':    
    lRecord = Record.load(sPath)
else:
    lRecord = Record.load_flatten_with_noisy_alignment(sPath)
N = len(lRecord)

#Index to partition data
lRecordIndex = [i for i in range(N)]

lRecordIndex = [lRecordIndex[i::number_of_folds] for i in range(number_of_folds)]
for i in range(number_of_folds):
    val_id = i
    test_id = (i+1)%number_of_folds
    lValid = [lRecord[j] for j in lRecordIndex[val_id]]
    lTest = [lRecord[j] for j in lRecordIndex[test_id]]
    remaining = []
    for j in range(number_of_folds):
        if j != val_id  and j != test_id:
            remaining.append(lRecordIndex[j])
    remaining =  reduce(lambda x,y: x+y, remaining)
    lTrain = [lRecord[j] for j in remaining]

    sPickleFile = oPath + base_filename + str(i) +".pkl"
    assert len(lTrain) + len(lValid) + len(lTest) == len(lRecord)

    oPartition = (lTrain , lValid , lTest)
    print("SAVING into %s" % sPickleFile)
    with open(sPickleFile, "wb") as fd:
        pickle.dump(oPartition, fd, protocol=2)
