# -*- coding: utf-8 -*-

"""
Starting point of the code which calls modules for feature extraction, 
trains the specified model with the data and calls the module to generate
the error analysis.

This file also has a counter-part (train.py) for crf models using pystuct.

@author Animesh Prasad
@data March 2018
"""
import numpy as np
import pickle as p

from utils import make_node_features as ngramfeatures
from utils_neural import make_node_features
from utils_neural import do_label_product, do_label_linearisation, open_tuples, make_tuples
from Esposalles.Record import Record

from model_neural import crf, blstm_crf, blstm, crf_we, blstm_we, blstm_crf_we, predict, blstm_multitask, predict_multitask

MAX_ITER = 200
C = .1
FOLDS = 10

def discard_padding(y_pr, y_t):
    list_to_return = []
    for items_pred, items_true in zip(y_pr, y_t):
        list_to_return.append(items_pred[:len(items_true)]) 
    return list_to_return

def transform_to_onehot(y_tr, class_length):
    list_to_return = []
    for items in y_tr:
        b =  np.zeros((items.shape[0], class_length))
        b[np.arange(items.shape[0]), items] = 1
        list_to_return.append(b)
    return list_to_return

def transform_from_onehot(y_p, class_length):
    list_to_return = []
    for items in y_p:
        a = np.array([np.where(r==max(r))[0][0] for r in items])
        list_to_return.append(a)
    return list_to_return

def do_padding(unpadded_list, depth=2, max_list_per_depth=[30, 10]):
    sample_lists = []
    for samples in unpadded_list:
        token_feature_list = []
        if depth == 2: 
            for tokens in samples:
                l = np.zeros(max_list_per_depth[1], dtype=np.int32)
                l[:len(tokens)] = np.array(tokens)
                token_feature_list.append(l)
            sample_lists.append(np.array(token_feature_list +  [np.zeros(max_list_per_depth[1], dtype=np.int32) for i in range(max_list_per_depth[0] - len(token_feature_list))]))
        elif depth == 1:
            l = np.zeros(max_list_per_depth[0], dtype=np.int32)
            l[:len(samples)] = np.array(samples)
            sample_lists.append(l)
    return np.array(sample_lists)


def train(model_id=-1):

    def augment_data(x, xN):
        a = []
        for item in [x, xN]:
            for i in item:
                a.append(np.array(i))
        return np.array(a)

    for label_type in ['cat', 'oth']:
        class_length = len(Record(0).class_dict[label_type])
        for folds in range (FOLDS):
            if model_id == -1 or model_id == 0:
                x_tr, x_v, x_t, y_tr, y_v, y_t, feature_extractor = ngramfeatures(label_type, folds)
                xN_tr, xN_v, xN_t, yN_tr, yN_v, yN_t, feature_extractor = ngramfeatures(label_type, folds, feature_extractor, True)

                train_set = {
                             'true' : (x_tr, y_tr),
                             'noisy' : (xN_tr, yN_tr),
                             'mix' : (augment_data(x_tr , xN_tr), augment_data(y_tr , yN_tr))
                            }

                test_set = {
                            'true' : (x_t, y_t),
                            'noisy' : (xN_t, yN_t)
                           }

                for train_name, (x_tr, y_tr) in train_set.items():
                    y_tr = transform_to_onehot(y_tr, class_length)
                    for model_name_str, model_name in {'crf':crf, 'blstm':blstm, 'blstm_crf':blstm_crf}.items():
                        model = model_name(feature_extractor.get_feature_names(), class_length, x_tr, y_tr)
                        for test_name, (x_t, y_t) in test_set.items():
                            y_pr = predict(model, x_t)
                            y_pr = transform_from_onehot(y_pr, class_length)
                            with open('outputs/' +  model_name_str + '_' + label_type + '_' + train_name + 'ON' + test_name  + '_' + str(folds), 'wb') as fp:
                                p.dump((y_t, y_pr), fp)

            if model_id == -1 or model_id == 1:
                x_tr, x_v, x_t, y_tr, y_v, y_t, feature_extractor, max_list_per_depth = make_node_features(label_type, folds)
                xN_tr, xN_v, xN_t, yN_tr, yN_v, yN_t, feature_extractor, max_list_per_depthN = make_node_features(label_type, folds, feature_extractor, True)
                max_list_per_depth = max(max_list_per_depth, max_list_per_depthN)

                train_set = {
                             'true' : (x_tr, y_tr),
                             'noisy' : (xN_tr, yN_tr),
                             'mix' : (augment_data(x_tr , xN_tr), augment_data(y_tr , yN_tr))
                            }

                test_set = {
                            'true' : (x_t, y_t),
                            'noisy' : (xN_t, yN_t)
                           }

                for train_name, (x_tr, y_tr) in train_set.items():
                    x_tr = do_padding(x_tr, 2, max_list_per_depth)
                    y_tr = do_padding(y_tr, 1, max_list_per_depth)
                    y_tr = transform_to_onehot(y_tr, class_length)
                    for model_name_str, model_name in {'blstm_cnn_we':blstm_cnn_we, 'crf_we':crf_we, 'blstm_we':blstm_we}.items():
                        model = model_name(feature_extractor.get_feature_names(), class_length, x_tr, y_tr)
                        for test_name, (x_t, y_t) in test_set.items(): 
                            x_t = do_padding(x_t, 2, max_list_per_depth)
                            y_pr = predict(model, x_t)
                            y_pr = transform_from_onehot(y_pr, class_length)
                            y_pr = discard_padding(y_pr, y_t)
                            with open('outputs/' +  model_name_str + '_' + label_type + '_' + train_name + 'ON' + test_name  + '_' + str(folds), 'wb') as fp:
                                p.dump((y_t, y_pr), fp)


    class_length1 = len(Record(0).class_dict['cat'])
    class_length2 = len(Record(0).class_dict['oth'])

    for folds in range (FOLDS):
        x1_tr, x1_v, x1_t, y1_tr, y1_v, y1_t, feature_extractor = ngramfeatures('cat', folds)
        _, _, _, y2_tr, y2_v, y2_t, _ = ngramfeatures('oth', folds)
        xN1_tr, xN1_v, xN1_t, yN1_tr, yN1_v, yN1_t, feature_extractor = ngramfeatures('cat', folds, feature_extractor, True)
        _, _, _, yN2_tr, yN2_v, yN2_t, _ = ngramfeatures('oth', folds, feature_extractor, True)

        train_set = {
                     'true' : (x1_tr, y1_tr, y2_tr),
                     'noisy' : (xN1_tr, yN1_tr, yN2_tr),
                     'mix' : (augment_data(x1_tr , xN1_tr), augment_data(y1_tr , yN1_tr), augment_data(y2_tr, yN2_tr))
                    }

        test_set = {
                    'true' : (x1_t, y1_t, y2_t),
                    'noisy' : (xN1_t, yN1_t, yN2_t)
                   }

        label_type =  'joint'
        if model_id == -1 or model_id == 2:
            for train_name, (x1_tr, y1_tr, y2_tr) in train_set.items():
                temp=Record(0)
                y_joint_tr = do_label_product(y1_tr, y2_tr, temp)
                y_joint_tr = transform_to_onehot(y_joint_tr, len(temp.active_dJOINT_index))
                y1_tr = transform_to_onehot(y1_tr, class_length1)
                y2_tr = transform_to_onehot(y2_tr, class_length2)
                for test_name, (x1_t, y1_t, y2_t) in test_set.items():
                    y_joint_t = do_label_product(y1_t, y2_t, temp)
                    for model_name_str, model_name in {'crf':crf, 'blstm':blstm, 'blstm_crf':blstm_crf}.items():
                        try:
                            model = model_name(feature_extractor.get_feature_names(), len(temp.active_dJOINT_index), x1_tr, y_joint_tr)
                            for test_name, (x1_t, y_joint_t) in test_set.items():
                                y_pr = predict(model, x1_t)
                                y_pr = transform_from_onehot(y_pr, len(temp.active_dJOINT_index))
                                with open('outputs/' +  model_name_str + '_' + label_type + '_' + train_name + 'ON' + test_name  + '_' + str(folds), 'wb') as fp:
                                    p.dump((y_joint_t, y_pr), fp)
                        except ValueError:
                            print('This fold is not trained-evaluated because because of 0 training sample for a class')

                    model = blstm_multitask(feature_extractor.get_feature_names(), class_length1, class_length2, x1_tr, y1_tr, y2_tr)
                    y1_pr, y2_pr = predict_multitask(model, x1_t)
                    y1_pr = transform_from_onehot(y1_pr, class_length1)
                    y2_pr = transform_from_onehot(y2_pr, class_length2)
                    with open('outputs/' +  'blstm_multitask1' + '_' + label_type + '_' + train_name + 'ON' + test_name  + '_' + str(folds), 'wb') as fp:
                        p.dump((y1_t, y1_pr), fp)
                    with open('outputs/' +  'blstm_multitask2' + '_' + label_type + '_' + train_name + 'ON' + test_name  + '_' + str(folds), 'wb') as fp:
                        p.dump((y2_t, y2_pr), fp)


if __name__ == "__main__":
    train(-1)

