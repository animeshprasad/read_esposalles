# -*- coding: utf-8 -*-

"""
General utilities

@author: Animesh Prasad
@date: 7 Mar 2018
"""
import os
import pickle
import random

from Esposalles.Record import Record
from Esposalles.Feature import Feature
from functools import reduce
import numpy as np


def do_label_product(label_list1, label_list2, record_object):
    """
    Produces a joint label list give two different label lists of same length.
    The joint list is re-indexed, and stored in a dummmy record object.
    
    The dummy object must have scope for all the data (lets say train and val)
    to be indexed by this mapping.
    
    This is required since pystruct require continous label range.  
    New indices starts at 0 and omit non-existent label combinations.

    TODO: Do we need support for more than two label types?  
    """
    cross_label_list=[]
    return_cross_label_list=[]
    temp = record_object
    class_lengths = []
    for key in temp.class_dict:
        class_lengths.append(len(temp.class_dict[key]))
    def mul(x,y): return x*y 
    cross_label_length=reduce(mul, class_lengths, 1)
    for label1,label2 in zip(label_list1,label_list2):
        l=[]
        for i in range(len(label1)):
            l.append(label1[i]*class_lengths[1]+label2[i])
        assert(len(l) == len(label1))
        cross_label_list.append(l)
    cross_label_set = set(reduce(lambda x,y: x+y, cross_label_list))
    # Add an new mapping!
    record_object.reindexJoint(cross_label_set)
    for label_points in cross_label_list:
        return_cross_label_list.append(np.array([record_object.active_dJOINT_index[s] for s in label_points]))
    return return_cross_label_list


def do_label_linearisation(first, label_list1, label_list2, record_object):
    """
    Produces a linearised label list give two different label lists of same length.
    The linerisation results into the class for label_list2 to start being indexed 
    from 0+len(n_states1) rather than from 0, where n_states1 is number of unique
    labels for label_list1.
    """
    new_start_index = len(record_object.class_dict[first])
    label_list2 = [x + new_start_index for x in label_list2]
    return [np.hstack((label_list1[i], label_list2[i])) for i in range(len(label_list1))]



def make_node_features(label_type='cat', folds=0, extractor=None, noisy=False):
    """
    Reads the data and label from the pickle file indicated by the folds
    and calls an instance of feature to index the train val and test using
    same indexing dictionary.
    """
    pickle_file_name = 'partitionN' if noisy else 'partition'

    f = open('data_folds/'+ pickle_file_name + str(folds) + '.pkl', 'rb')

    train_records, test_records, val_records = pickle.load(f, encoding='utf-8')

    method = '1-crf'
    train, val, test   =  None, None, None
    if method[:2] == 'crf':
        train =  train_records +  val_records
    else:
        train = train_records
        val = val_records
    test = test_records


    def record_to_token_lists(split):
        vocab = []
        label = []
        unique = set()
        to_model = []
        for items in split:
            list_of_tokens=reduce(lambda x,y: x+y,items.getRecordText())
            list_of_labels=reduce(lambda x,y: x+y,items.getRecordLabel(label_type))
            vocab.append(list_of_tokens)
            label.append(list_of_labels)
            to_model.append(' '.join(list_of_tokens))
        unique = set(reduce(lambda x,y: x+y, vocab))
        return (vocab, label, unique)


    train_vocab, train_label, unique_train = record_to_token_lists(train_records)
    if val is not None:
       val_vocab, val_label, _ = record_to_token_lists(val_records)
    test_vocab, test_label, _ = record_to_token_lists(test_records)
       

    # Index it!
    features = Feature()
    if extractor:
        features.set_feature_extractor(extractor)
    x_tr, list_max_tr=features.fit_index_on_X(train_vocab, unique_train)
    x_t, list_max_t=features.fit_index_on_X(test_vocab)
    y_tr=features.fit_Y(train_label)
    y_t=features.fit_Y(test_label)
    x_v =  None
    y_v = None
    feature_extractor = features.get_feature_extractor()
    if val:
        x_v, list_max_v=np.array(features.fit_index_on_X(val_vocab))
        y_v=np.array(features.fit_Y(val_label))

    list_max = [max([a,b]) for a,b in zip(list_max_tr, list_max_t) ]

    l = [zip(x_tr, y_tr), zip(x_t, y_t), zip(x_v, y_v)]
    for i in l:
        for data,labels in i:
            assert(len(data) ==  len(labels))
    return (x_tr, x_v, x_t, y_tr, y_v, y_t, feature_extractor, list_max)
 

def make_links_for_graph(feature_list, label_type='cat', link_type='skipped', skip=[1,5]):
    """
    Takes a list of links per record returns the feature_list with the links.
    feature_list: is needed to know the number of tokens in individual records.
    link_type: provide various ways to encode or represent the edges
    skip: is length of the lik to be considered can be an integer or a list in 
          case of multiple links.
    """
    link_appended_feature_list = []

    def add_skipped(skip):
        """
        undirectional edge to skip'th neighbour node
        """
        edge_feature_length = feature_vectors.shape[0]-skip
        links = np.zeros((edge_feature_length, 2), np.int32)
        links[:,0] = np.arange(0, edge_feature_length)
        links[:,1] = np.arange(skip, edge_feature_length+skip)
        return links

    def add_normalised_skipped():
        """
        undirectional edge to k'th neighbours wher k is dynamically decided based on the sequence
        length and the label size
        """
        temp = Record(0)
        label_length = len(temp.class_dict[label_type])
        c_skip = feature_vectors.shape[0]//label_length
        if c_skip > skip:
            c_skip = skip
        edge_feature_length = feature_vectors.shape[0] - c_skip
        links = np.zeros((edge_feature_length, 2), np.int32)
        links[:,0] = np.arange(0, edge_feature_length)
        links[:,1] = np.arange(c_skip, edge_feature_length + c_skip)
        return links
    
    def skipped():
        """
        wrapper for skipped, also take cares of multiple skip links.
        skip = 1 is added always, if specified skip is an integer 
        only skips of specified lengths with be added if skip is a list 
        """
        if type(skip) == int:
            #Link of size 1 will be added automatically
            return np.vstack([add_skipped(1), add_skipped(skip)])
        elif len(skip) == 1:
            #This is the way of only having size k link
            return add_skipped(skip[0])
        else:
            return np.vstack([add_skipped(i) for i in skip])

    def normalised_skipped():
        """
        wrapper for skipped and normalised_skipped
        skip = 1 is added always 
        """
        return np.vstack([add_skipped(1), add_normalised_skipped()])


    link_options = {'skipped':skipped,
                    'normalised_skipped':normalised_skipped}

    for feature_vectors in feature_list:
        links = link_options[link_type]()
        link_appended_feature_list.append((feature_vectors, links))
    return link_appended_feature_list


def add_features_to_links(link_appended_feature_list, skip=6):
    """
    Augument the identified edge links with features. The representation 
    uses one hot encoding for length as features.
    """
    link_augmented_feature_list = []
    for feature_edge_tuple in link_appended_feature_list:
        feature_vectors, links = feature_edge_tuple
        link_features = np.zeros((links.shape[0], skip), np.int32)
        for i in range (links.shape[0]):
            link_features[i][ links[i][1]-links[i][0]] =  1
        link_augmented_feature_list.append((feature_vectors, links, link_features))
    return link_augmented_feature_list

def open_tuples(list_of_tuples):
    """
    Convert a list of tuples to tuple of lists
    """
    list = []
    for i in range(len(list_of_tuples[0])):
        list.append([x[i] for x in list_of_tuples])
    return tuple(list[i] for i in range((len(list))))
        
def make_tuples(l_node_features, l_edges, l_edge_features, n_types):
    """
    Make tuples from lists 
    """
    assert(len(l_node_features[0]) == len(l_edges[0]))
    assert(len(l_edges[0]) == len(l_edge_features[0]))
    x_tuple = []
    for i in range(len(l_node_features[0])):
        x_tuple.append(([l_node_features[j][i] for j in range(n_types)],
                       [l_edges[j][i] for j in range(n_types*n_types)],
                       [l_edge_features[j][i] for j in range(n_types*n_types)]))
    return x_tuple

if __name__ == "__main__":
    make_features()
